﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SZA.Startup))]
namespace SZA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
