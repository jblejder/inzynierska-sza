﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SZA.Models
{
    
    public class VendingMachine
    {
        public virtual int VendingMachineId { get; set; }
        public virtual int OwnerId { get; set; }
        public virtual List<Product> Products { get; set; }
        public virtual Owner Owner { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Money { get; set; }

        public VendingMachine()
        {

        }
    }
}