﻿using System;

namespace SZA.Models
{
    public class Product
    {
        public virtual int ProductId { get; set; }
        public virtual string Name { get; set; }
        public virtual decimal Price { get; set; }
        public virtual int Count { get; set; }

        public Product()
        {

        }
    }
}