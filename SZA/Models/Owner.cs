﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SZA.Models
{
    public class Owner
    {
        public virtual int OwnerId{ get; set; }
        public virtual string Name { get; set; }

        public Owner()
        {

        }
    }
}