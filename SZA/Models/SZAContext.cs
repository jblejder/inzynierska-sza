﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SZA.Models
{
    public class SZAContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public SZAContext() : base("name=SZAContext")
        {
            Database.SetInitializer<SZAContext>(new SZAContextInitializer());
        }

        public System.Data.Entity.DbSet<SZA.Models.VendingMachine> VendingMachines { get; set; }

        public System.Data.Entity.DbSet<SZA.Models.Owner> Owners { get; set; }

        public System.Data.Entity.DbSet<SZA.Models.Product> Products { get; set; }
    }
}
