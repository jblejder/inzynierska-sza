﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SZA.Models
{
    public class SZAContextInitializer
        : System.Data.Entity.DropCreateDatabaseAlways<SZAContext>
    {
        protected override void Seed(SZAContext context)
        {
            context.Owners.Add(new Owner
            {
                OwnerId = 1,
                Name = "GoodCompany"
            });

            context.Owners.Add(new Owner
            {
                OwnerId = 2,
                Name = "HappyCompany"
            });
            context.VendingMachines.Add(new VendingMachine
            {
                VendingMachineId = 1,
                OwnerId = 1,
                Description = "Obok sklepu spozywczego",
                Money = 100.00m,
                Products = new List<Product> {
                new Product {Name ="Nuca-cola", Price = 2.99m, Count = 10},
                new Product {Name ="Snaki", Price = 5.99m, Count = 10 }
                }

            });
            context.VendingMachines.Add(new VendingMachine
            {
                VendingMachineId = 2,
                OwnerId = 2,
                Description = "W centrum miasta",
                Money = 50.20m,
                Products = new List<Product> {
                new Product {Name ="Nuca-cola", Price = 5.99m, Count = 10},
                new Product {Name ="Snaki", Price = 3.99m, Count = 10 }
                }
            });
            base.Seed(context);
        }
    }
}