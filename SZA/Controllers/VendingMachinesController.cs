﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SZA.Models;

namespace SZA.Controllers
{
    public class VendingMachinesController : Controller
    {
        private SZAContext db = new SZAContext();

        // GET: VendingMachines
        public ActionResult Index()
        {
            var vendingMachines = db.VendingMachines.Include(v => v.Owner);
            return View(vendingMachines.ToList());
        }

        // GET: VendingMachines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendingMachine vendingMachine = db.VendingMachines.Include(v => v.Products).SingleOrDefault(v => v.VendingMachineId == id);
            
            if (vendingMachine == null)
            {
                return HttpNotFound();
            }
            return View(vendingMachine);
        }

        // GET: VendingMachines/Create
        public ActionResult Create()
        {
            ViewBag.OwnerId = new SelectList(db.Owners, "OwnerId", "Name");
            return View();
        }

        // POST: VendingMachines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VendingMachineId,OwnerId,Description")] VendingMachine vendingMachine)
        {
            if (ModelState.IsValid)
            {
                db.VendingMachines.Add(vendingMachine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OwnerId = new SelectList(db.Owners, "OwnerId", "Name", vendingMachine.OwnerId);
            return View(vendingMachine);
        }

        // GET: VendingMachines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendingMachine vendingMachine = db.VendingMachines.Find(id);
            if (vendingMachine == null)
            {
                return HttpNotFound();
            }
            ViewBag.OwnerId = new SelectList(db.Owners, "OwnerId", "Name", vendingMachine.OwnerId);
            return View(vendingMachine);
        }

        // POST: VendingMachines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VendingMachineId,OwnerId,Description")] VendingMachine vendingMachine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vendingMachine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OwnerId = new SelectList(db.Owners, "OwnerId", "Name", vendingMachine.OwnerId);
            return View(vendingMachine);
        }

        // GET: VendingMachines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendingMachine vendingMachine = db.VendingMachines.Find(id);
            if (vendingMachine == null)
            {
                return HttpNotFound();
            }
            return View(vendingMachine);
        }

        // POST: VendingMachines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VendingMachine vendingMachine = db.VendingMachines.Find(id);
            db.VendingMachines.Remove(vendingMachine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
