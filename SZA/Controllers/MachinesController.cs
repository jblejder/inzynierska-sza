﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SZA.Models;

namespace SZA.Controllers
{
    public class MachinesController : ApiController
    {
        private SZAContext db = new SZAContext();


        // GET: api/Machines
        public IQueryable<VendingMachine> GetVendingMachines()
        {
            return db.VendingMachines;
        }

        // GET: api/Machines/5
        [ResponseType(typeof(VendingMachine))]
        public IHttpActionResult GetVendingMachine(int id)
        {
            VendingMachine vendingMachine = db.VendingMachines.Find(id);
            if (vendingMachine == null)
            {
                return NotFound();
            }

            return Ok(vendingMachine);
        }

        // PUT: api/Machines/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVendingMachine(int id, VendingMachine vendingMachine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vendingMachine.VendingMachineId)
            {
                return BadRequest();
            }

            db.Entry(vendingMachine).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendingMachineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Machines
        [ResponseType(typeof(VendingMachine))]
        public IHttpActionResult PostVendingMachine(VendingMachine vendingMachine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VendingMachines.Add(vendingMachine);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vendingMachine.VendingMachineId }, vendingMachine);
        }

        // DELETE: api/Machines/5
        [ResponseType(typeof(VendingMachine))]
        public IHttpActionResult DeleteVendingMachine(int id)
        {
            VendingMachine vendingMachine = db.VendingMachines.Find(id);
            if (vendingMachine == null)
            {
                return NotFound();
            }

            db.VendingMachines.Remove(vendingMachine);
            db.SaveChanges();

            return Ok(vendingMachine);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VendingMachineExists(int id)
        {
            return db.VendingMachines.Count(e => e.VendingMachineId == id) > 0;
        }
    }
}