﻿using System;
using System.IO;

namespace Recorder
{
    public class Logger
    {
        #region Private Fields
        private static string folderPath;
        private static StreamWriter summaryFile;
        private StreamWriter logFile;

        private string _Name;
        #endregion

        #region Properties
        public string Name { get { return _Name; } }
        #endregion

        #region Constructors
        public Logger(string name)
        {
            Initialize();
            InitializeSummaryWriter();
            while (File.Exists(folderPath + name))
            {
                name += "$";
            }
            _Name = name;
            logFile = new StreamWriter(folderPath + _Name + ".txt", false);
        }
        #endregion

        #region Private Methods
        private bool Initialize()
        {
            if (folderPath != null)
            {
                return true;
            }
            string logsPath = Path.Combine(Environment.CurrentDirectory, @"..\..\Logs\");
            string controllFile = logsPath + @"LogControll.txt";
            Console.Write(controllFile);
            if (!Directory.Exists(logsPath))
            {
                Directory.CreateDirectory(logsPath);
            }
            if (!File.Exists(controllFile))
            {

                StreamWriter init = new StreamWriter(controllFile);
                init.WriteLine("0");
                init.Dispose();
            }
            StreamReader sr = new StreamReader(controllFile);
            int number = int.Parse(sr.ReadLine());
            sr.Dispose();
            number++;
            folderPath = logsPath + number.ToString() + @"\";
            Directory.CreateDirectory(logsPath + number);
            StreamWriter sw = new StreamWriter(controllFile, false);
            sw.Write(number);
            sw.Dispose();
            return true;
        }

        private void InitializeSummaryWriter()
        {
            if (summaryFile != null)
                return;
            summaryFile = new StreamWriter(folderPath + "summaryLog.txt");
            summaryFile.Write("Summary Log File Started");
        }

        #endregion

        public void WriteLog(string message)
        {
            logFile.WriteLine(DateTime.Now + " :\t" + message + '\n');
            logFile.Flush();

            summaryFile.WriteLine(DateTime.Now + " : " + Name + " :\t" + message);
            summaryFile.Flush();
        }

    }
}
