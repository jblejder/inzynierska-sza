Trello - https://goo.gl/u0nSMU
### Co tu się dzieje? ###

Repozytorium dla mojej pracy inżynierskiej.
Solucja obecnie zawiera 3 projekty.

**SZA** - Jest go główny projekt serwisu webowego. Obecnie w stanie bardzo wczesnej produkcji.

**VendingSim** - Projekt pomocny. Można w nim tworzyć symulowane maszyny sprzedażowe umożliwiając testy.

**Recorder** - Projekt biblioteka zawierający narzędzia do prowadzenia logów.

### Jak uruchomić? ###

* U mnie działa... Nie powinno być narazie żadnych problemów z uruchomieniem aplikacji.

### Co jest zrobione? ###

* Wstępne projekty.
* SZA:
* * Model bazy danych i połączenie poprzez EntityFramework
* * Wstępnie przygotowane kontrolery i widoki 
* * * /VendingMachines
* * * /Owners
* * * /Products
* VendingSim
* * Można dodawać i usuwać maszyny
* * Używa logów

### Co nad czym obecna praca? ###

* Stworzenie pierwszeto API i podłączenie symulowanych maszyn by potrafiły się z nim komunikować.