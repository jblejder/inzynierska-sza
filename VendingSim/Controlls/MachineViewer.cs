﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Recorder;

namespace VendingSim.Controlls
{
    public partial class MachineViewer : UserControl
    {
        private static Logger logger = new Logger("MachineViewer");

        private VirtualVendMachine vendingMachine;
        private bool initialized;
        private bool valuesChanged;

        public MachineViewer(VirtualVendMachine machine)
        {
            InitializeComponent();
            vendingMachine = machine;
            RefreshValues();
            initialized = true;
            valuesChanged = false;
        }

        public void RefreshValues()
        {
            initialized = false;
            tbState.Text = vendingMachine.GetState.ToString();
            if (vendingMachine.VendingMachineId > 0)
                tbID.Text = vendingMachine.VendingMachineId.ToString();
            if (vendingMachine.Owner != null)
                tbName.Text = vendingMachine.Owner.Name;
            if (vendingMachine.Description != null)
                rtbDescription.Text = vendingMachine.Description;
            nInterval.Value = vendingMachine.Interval;
            cbIsRunning.Checked = vendingMachine.IsWorking;
            initialized = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!valuesChanged)
            {
                RefreshValues();
                logger.WriteLog("Odswierzono");
            }
        }

        private void cbIsRunning_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIsRunning.Checked)
            {
                this.vendingMachine.Run();
            }
            else
            {
                this.vendingMachine.Stop();
            }
            this.RefreshValues();
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            this.valuesChanged = true;
            bSaveChanges.Enabled = true;
            if (!initialized)
                return;
            Control c = (Control)sender;
            c.BackColor = Color.Orange;
        }

        private void bSaveChanges_Click(object sender, EventArgs e)
        {
            bSaveChanges.Enabled = false;
            cbIsRunning.Checked = false;
            vendingMachine.SetId(int.Parse(tbID.Text));
            vendingMachine.SetName(tbName.Text);
            vendingMachine.SetDescription(rtbDescription.Text);
            vendingMachine.SetInterval((int)nInterval.Value);
            RestoreColors();
            this.valuesChanged = false;
        }
        
        private void RestoreColors()
        {
            tbID.BackColor = Color.White;
            tbName.BackColor = Color.White;
            nInterval.BackColor = Color.White;
            rtbDescription.BackColor = Color.White;
        }
    }
}
