﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Recorder;
using Newtonsoft.Json;
using SZA.Models;

namespace VendingSim
{
    public class VirtualVendMachine : VendingMachine
    {
        public enum State { Wait, Work, Error };

        #region Fields
        private Logger logger;
        private DateTime lastTime;
        private bool initialized;
        private State state;
        #endregion

        #region Constructors
        public VirtualVendMachine(string name)
        {
            this.initialized = false;
            this.VendingMachineId = 1;
            this.Name = name;
            this.lastTime = DateTime.Now;
            this.logger = new Logger(name + "-VirtualVendMachine");
            this.state = State.Wait;
        }
        #endregion

        #region Properties
        public string Name { get; set; }
        public int Interval { get; set; }
        public bool GetInitialized { get { return initialized; } }
        public bool IsWorking { get { return (state == State.Work) ? true : false; } }
        public State GetState { get { return state; } }
        #endregion

        #region Public Methods
        public override string ToString()
        {
            return Name;
        }

        public void Run()
        {
            if (state != State.Work)
            {
                state = State.Work;
                logger.WriteLog("Machine started");
            }
        }
        public void Stop()
        {
            if (state != State.Wait && state != State.Error)
            {
                state = State.Wait;
                logger.WriteLog("Machine Stopped");
            }
        }

        public void IntervalElapsed()
        {
            if (state == State.Work)
            {
                DateTime now = DateTime.Now;
                TimeSpan ts = now - lastTime;
                if (ts.TotalMilliseconds > Interval)
                {
                    lastTime = now;
                    logger.WriteLog("Time now: " + now + ".");
                    if (!this.initialized)
                    {
                        if (!this.FirstConnection())
                        {
                            state = State.Error;
                            logger.WriteLog("Stopped. Initialization failure.");
                        }
                    }
                }
            }
        }

        public void SetId(int id)
        {
            if (id == this.VendingMachineId)
                return;
            this.VendingMachineId = id;
            this.initialized = false;
        }

        public void SetName(string name)
        {
            this.Name = name;
        }

        public void SetDescription(string desc)
        {
            this.Description = desc;
        }
        public void SetInterval(int interval)
        {
            this.Interval = interval;
        }
        #endregion

        #region Private Methods
        private void ApplyValues(VendingMachine vm)
        {
            this.OwnerId = vm.OwnerId;
            this.Products = vm.Products;
            this.Owner = vm.Owner;
            this.Description = vm.Description;
        }

        private bool FirstConnection()
        {
            WebClient wc = new WebClient();
            string api = Program.Address + VendingMachineId;
            try
            {
                string get = wc.DownloadString(api);
                VendingMachine gotvm = JsonConvert.DeserializeObject<VendingMachine>(get);
                this.ApplyValues(gotvm);
                this.logger.WriteLog("Pobrano dane.\t");
                this.initialized = true;
                return true;
            }
            catch (Exception e)
            {
                logger.WriteLog("Blad pobrania danych.");
                return false;
            }
        }
        #endregion
    }
}
