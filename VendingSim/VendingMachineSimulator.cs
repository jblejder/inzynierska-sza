﻿namespace VendingSim
{
    using System;
    using System.ComponentModel;
    using System.Timers;
    using Recorder;

    class VendingMachineSimulator
    {
        public delegate void TimerTickEvendHandler(object sender, EventArgs args);
        public event TimerTickEvendHandler TimerTickEvent;

        public BindingList<VirtualVendMachine> machines { get; set; }
        public double TimerInterval { get { return timer.Interval; } set { timer.Interval = value; } }

        private Timer timer;

        public VendingMachineSimulator()
        {
            this.InitializeTimer();
            this.machines = new BindingList<VirtualVendMachine>();
        }

        public void AddMachine(VirtualVendMachine vvm)
        {
            int maxIndex = 0;
            foreach (var item in this.machines)
            {
                if (maxIndex < item.VendingMachineId)
                    maxIndex = item.VendingMachineId;
            }
            vvm.VendingMachineId = maxIndex + 1;
            this.machines.Add(vvm);
        }

        public void RemoveMachine(int index)
        {
            this.machines.RemoveAt(index);
        }

        public VirtualVendMachine GetMachine(int index)
        {
            return this.machines[index];
        }

        private void InitializeTimer()
        {
            timer = new Timer(500);
            timer.Elapsed += new ElapsedEventHandler((x, y) =>
            {
                if (TimerTickEvent != null)
                    TimerTickEvent(this, EventArgs.Empty);
                foreach (var machine in machines)
                {
                    machine.IntervalElapsed();
                }
            });
            timer.Start();
        }
    }
}
