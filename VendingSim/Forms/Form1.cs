﻿using Recorder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VendingSim.Controlls;

namespace VendingSim
{
    public partial class Form1 : Form
    {
        VendingMachineSimulator sim = new VendingMachineSimulator();
        Logger logger = new Logger("Forms");


        MachineViewer mv;
        
        int nr = 2;
        public Form1()
        {
            logger.WriteLog("Application Started");
            InitializeComponent();
            MachineList.DataSource = sim.machines;
            //sim.TimerTickEvent += new VendingMachineSimulator.TimerTickEvendHandler((a, b) =>
            //{
            //    if(mv != null)
            //    {
            //        mv.Refresh();
            //    }
            //});
        }

        private void AddSim_Click(object sender, EventArgs e)
        {
            string name = "R2D" + nr;
            sim.AddMachine(new VirtualVendMachine(name) {Interval = 1000});
            logger.WriteLog("Machine " + name + " started");
            nr++;
            RefreshMachineViewer();
        }

        private void RemoveMachine_Click(object sender, EventArgs e)
        {

            if (MachineList.SelectedIndex > -1)
            {
                int index = MachineList.SelectedIndex;
                string name = sim.GetMachine(index).Name;
                sim.RemoveMachine(MachineList.SelectedIndex);
                logger.WriteLog("Machine " + name + " removed");
            }
            if (MachineList.Items.Count > 0)
                MachineList.SelectedIndex = 0;
            RefreshMachineViewer();
        }

        private void MachineList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshMachineViewer();
        }

        private void RefreshMachineViewer()
        {
            if (mv != null)
                mv.Dispose();
            if (MachineList.SelectedIndex > -1)
            {
                mv = new MachineViewer(sim.GetMachine(MachineList.SelectedIndex));
                mv.Dock = DockStyle.Fill;
                tableLayoutPanel1.Controls.Add(mv, 2, 0);
            }
        }
    }
}
